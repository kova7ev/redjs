package com.ecwid.redjs.transport;

public abstract class Transport {
	private String mHost;
	private int mPort;
	
	public Transport(String host, int port){
		mHost = host;
		mPort = port;
	}
	
	public String getHost(){
		return mHost;
	}
	
	public int getPort(){
		return mPort;
	}
	
	public abstract String send(String query);
}
