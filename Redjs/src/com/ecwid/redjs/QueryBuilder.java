package com.ecwid.redjs;

import java.util.HashMap;
import java.util.Map;

public class QueryBuilder {
	
	private Query mQuery;
	private Map<Params, Object> mParams;
	
	public QueryBuilder(){
		mParams = new HashMap<Params, Object>();
	}
	
	public void addQuery(Query query){
		mQuery = query;
	}
	
	public void addParam(Params param, Object value){
		mParams.put(param, value);
	}
	
	public String build(){
		if (mQuery == Query.HSET){
			//C: *3\r\n
			//C: $4\r\n
			//C: HSET\r\n
			//C: $num_bytes_in_hash_name\r\n
			//C: hash_name\r\n
			//C: $num_bytes_in_key\r\n
			//C: key\r\n
			//C: $num_bytes_in_value\r\n
			//C: value\r\n
			
			StringBuilder builder = new StringBuilder();
			builder.append("*4\r\n");
			builder.append("$4\r\n");
			builder.append("HSET\r\n");
			builder.append(String.format("$%d\r\n", mParams.get(Params.KEY).toString().length()));
			builder.append(String.format("%s\r\n", mParams.get(Params.KEY).toString()));
			builder.append(String.format("$%d\r\n", mParams.get(Params.FIELD).toString().length()));
			builder.append(String.format("%s\r\n", mParams.get(Params.FIELD).toString()));
			builder.append(String.format("$%d\r\n", mParams.get(Params.VALUE).toString().length()));
			builder.append(String.format("%s\r\n", mParams.get(Params.VALUE).toString()));
			
			return builder.toString();
		}else if (mQuery == Query.HGET){
			//C: *3\r\n
			//C: $4\r\n
			//C: HGET\r\n
			//C: $num_bytes_in_hash_name\r\n
			//C: hash_name\r\n
			//C: $num_bytes_in_key\r\n
			//C: key\r\n
			
			StringBuilder builder = new StringBuilder();
			builder.append("*3\r\n");
			builder.append("$4\r\n");
			builder.append("HGET\r\n");
			builder.append(String.format("$%d\r\n", mParams.get(Params.KEY).toString().length()));
			builder.append(String.format("%s\r\n", mParams.get(Params.KEY).toString()));
			builder.append(String.format("$%d\r\n", mParams.get(Params.FIELD).toString().length()));
			builder.append(String.format("%s\r\n", mParams.get(Params.FIELD).toString()));
			
			return builder.toString();
		}else if (mQuery == Query.SCAN){
			//C: *4\r\n
			//C: $4\r\n
			//C: SCAN\r\n
			//C: :0\r\n
			//C: $5\r\n
			//C: MATCH\r\n
			//C: $num_byte_in_name\r\n
			//C: name\r\n
			
			StringBuilder builder = new StringBuilder();
			builder.append("*4\r\n");
			builder.append("$4\r\n");
			builder.append("SCAN\r\n");
			builder.append(String.format("$%d\r\n", mParams.get(Params.CURSOR).toString().length()));
			builder.append(String.format("%s\r\n", mParams.get(Params.CURSOR).toString()));
			builder.append("$5\r\n");
			builder.append("MATCH\r\n");
			builder.append(String.format("$%d\r\n", mParams.get(Params.MATCH).toString().length()));
			builder.append(String.format("%s\r\n", mParams.get(Params.MATCH).toString()));
			
			return builder.toString();
		}else if (mQuery == Query.DEL){
			//C: *2\r\n
			//C: $3\r\n
			//C: DEL\r\n
			//C: $numb_bytes_in_name\r\n
			//C: name\r\n
			
			StringBuilder builder = new StringBuilder();
			builder.append("*2\r\n");
			builder.append("$3\r\n");
			builder.append("DEL\r\n");
			builder.append(String.format("$%d\r\n", mParams.get(Params.KEY).toString().length()));
			builder.append(String.format("%s\r\n", mParams.get(Params.KEY).toString()));
			
			return builder.toString();
		}else if (mQuery == Query.HEXISTS){
			//C: *3\r\n
			//C: $7\r\n
			//C: HEXISTS\r\n
			//C: $num_bytes_in_hash_name\r\n
			//C: hash_name\r\n
			//C: $num_bytes_in_key\r\n
			//C: key\r\n
			
			StringBuilder builder = new StringBuilder();
			builder.append("*3\r\n");
			builder.append("$7\r\n");
			builder.append("HEXISTS\r\n");
			builder.append(String.format("$%d\r\n", mParams.get(Params.KEY).toString().length()));
			builder.append(String.format("%s\r\n", mParams.get(Params.KEY).toString()));
			builder.append(String.format("$%d\r\n", mParams.get(Params.FIELD).toString().length()));
			builder.append(String.format("%s\r\n", mParams.get(Params.FIELD).toString()));
			
			return builder.toString();
		}else if (mQuery == Query.HVALS){
			//C: *2\r\n
			//C: $5\r\n
			//C: HVALS\r\n
			//C: $num_bytes_in_hash_name\r\n
			//C: hash_name\r\n
			
			StringBuilder builder = new StringBuilder();
			builder.append("*2\r\n");
			builder.append("$5\r\n");
			builder.append("HVALS\r\n");
			builder.append(String.format("$%d\r\n", mParams.get(Params.KEY).toString().length()));
			builder.append(String.format("%s\r\n", mParams.get(Params.KEY).toString()));
			
			return builder.toString();
		}else if (mQuery == Query.HGETALL){
			//C: *2\r\n
			//C: $7\r\n
			//C: HGETALL\r\n
			//C: $numb_bytes_in_name\r\n
			//C: name\r\n
			
			StringBuilder builder = new StringBuilder();
			builder.append("*2\r\n");
			builder.append("$7\r\n");
			builder.append("HGETALL\r\n");
			builder.append(String.format("$%d\r\n", mParams.get(Params.KEY).toString().length()));
			builder.append(String.format("%s\r\n", mParams.get(Params.KEY).toString()));
			
			return builder.toString();
		}else if (mQuery == Query.HKEYS){
			//C: *2\r\n
			//C: $5\r\n
			//C: HKEYS\r\n
			//C: $numb_bytes_in_name\r\n
			//C: name\r\n
			
			StringBuilder builder = new StringBuilder();
			builder.append("*2\r\n");
			builder.append("$5\r\n");
			builder.append("HKEYS\r\n");
			builder.append(String.format("$%d\r\n", mParams.get(Params.KEY).toString().length()));
			builder.append(String.format("%s\r\n", mParams.get(Params.KEY).toString()));
			
			return builder.toString();
		}else if (mQuery == Query.HDEL){
			//C: *3\r\n
			//C: $4\r\n
			//C: HDEL\r\n
			//C: $num_bytes_in_hash_name\r\n
			//C: hash_name\r\n
			//C: $num_bytes_in_key\r\n
			//C: key\r\n
			
			StringBuilder builder = new StringBuilder();
			builder.append("*3\r\n");
			builder.append("$4\r\n");
			builder.append("HDEL\r\n");
			builder.append(String.format("$%d\r\n", mParams.get(Params.KEY).toString().length()));
			builder.append(String.format("%s\r\n", mParams.get(Params.KEY).toString()));
			builder.append(String.format("$%d\r\n", mParams.get(Params.FIELD).toString().length()));
			builder.append(String.format("%s\r\n", mParams.get(Params.FIELD).toString()));
			
			return builder.toString();
		}else if (mQuery == Query.HLEN){
			//C: *2\r\n
			//C: $4\r\n
			//C: HLEN\r\n
			//C: $num_bytes_in_hash_name\r\n
			//C: hash_name\r\n
			
			StringBuilder builder = new StringBuilder();
			builder.append("*2\r\n");
			builder.append("$4\r\n");
			builder.append("HLEN\r\n");
			builder.append(String.format("$%d\r\n", mParams.get(Params.KEY).toString().length()));
			builder.append(String.format("%s\r\n", mParams.get(Params.KEY).toString()));
			
			return builder.toString();
		}
		
		return "";
	}
}