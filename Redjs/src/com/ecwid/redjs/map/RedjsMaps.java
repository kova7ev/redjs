package com.ecwid.redjs.map;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ecwid.redjs.Redjs;
import com.ecwid.redjs.Params;
import com.ecwid.redjs.Query;
import com.ecwid.redjs.QueryBuilder;

public class RedjsMaps {
	
	public RedjsMaps(){
		
	}

	public <K, V> RedjsMap<K, V> getMap(String name){
		if (existsMap(name)){
			return new RedjsMap<K, V>(name, false);
		}
		else{
			return null;
		}
	}
	
	public <K, V> RedjsMap<K, V> newMap(String name){
		return new RedjsMap<K, V>(name, true);
	}
	
	public boolean deleteMap(String name) {
		QueryBuilder query = new QueryBuilder();
		query.addQuery(Query.DEL);
		query.addParam(Params.KEY, name);
		
		String responce = Redjs.getTransport().send(query.build());
		return responce.equals(":1\r\n");
	}
	
	public <K, V> boolean deleteMap(RedjsMap<K, V> redjsMap) {
		return deleteMap(redjsMap.getName());
	}
	
	public boolean existsMap(String name) {
		QueryBuilder query = new QueryBuilder();
		query.addQuery(Query.SCAN);
		query.addParam(Params.CURSOR, 0);
		query.addParam(Params.MATCH, name);
		
		String responce = Redjs.getTransport().send(query.build());
		String regexp = "\\*2\\r\\n\\$1\\r\\n0\\r\\n\\*1";
		Pattern pattern = Pattern.compile(regexp);
		Matcher matcher = pattern.matcher(responce);

		if (matcher.find()) {
			return true;
		} else {
			return false;
		}
	}
}