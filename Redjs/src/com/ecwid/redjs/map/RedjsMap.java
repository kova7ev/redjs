package com.ecwid.redjs.map;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ecwid.redjs.Redjs;
import com.ecwid.redjs.Params;
import com.ecwid.redjs.Query;
import com.ecwid.redjs.QueryBuilder;

public class RedjsMap<K, V> implements Map<K, V> {
	private String mName;
	private final String EMPTY_FIELD = "is_empty";
		
	public RedjsMap(String name){
		this(name, false);
	}
	
	public RedjsMap(String name, boolean isEmpty){
		mName = name;

		if (isEmpty){
			QueryBuilder query = new QueryBuilder();
			query.addQuery(Query.HSET);
			query.addParam(Params.KEY, mName);
			query.addParam(Params.FIELD, EMPTY_FIELD);
			query.addParam(Params.VALUE, true);
			Redjs.getTransport().send(query.build());
		}
	}  

	public String getName() {
		return mName;
	}

	@Override
	public void clear() {
		QueryBuilder query = new QueryBuilder();
		query.addQuery(Query.DEL);
		query.addParam(Params.KEY, mName);
		Redjs.getTransport().send(query.build());

		query = new QueryBuilder();
		query.addQuery(Query.HSET);
		query.addParam(Params.KEY, mName);
		query.addParam(Params.FIELD, EMPTY_FIELD);
		query.addParam(Params.VALUE, true);
		Redjs.getTransport().send(query.build());
	}

	@Override
	public boolean containsKey(Object key) {
		QueryBuilder query = new QueryBuilder();
		query.addQuery(Query.HEXISTS);
		query.addParam(Params.KEY, mName);
		query.addParam(Params.FIELD, key);
		String responce = Redjs.getTransport().send(query.build());

		return responce.equals(":1\r\n");
	}

	@Override
	public boolean containsValue(Object value) {
		QueryBuilder query = new QueryBuilder();
		query.addQuery(Query.HVALS);
		query.addParam(Params.KEY, mName);
		String responce = Redjs.getTransport().send(query.build());

		String regexp = String.format("\\$%d\\r\\n%s\\r\\n", value.toString()
				.length(), value.toString());
		Pattern pattern = Pattern.compile(regexp);
		Matcher matcher = pattern.matcher(responce);

		if (matcher.find()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Set<java.util.Map.Entry<K, V>> entrySet() {
		QueryBuilder query = new QueryBuilder();
		query.addQuery(Query.HGETALL);
		query.addParam(Params.KEY, mName);
		String responce = Redjs.getTransport().send(query.build());

		String[] terms = responce.split("\r\n");
		Set<java.util.Map.Entry<K, V>> enrySet = new HashSet<>();

		for (int i = 2; i < terms.length; i = i + 4) {
			if (terms[i].equals(EMPTY_FIELD)) {
				continue;
			}
			K key = (K) parseValue(terms[i]);
			V value = (V) parseValue(terms[i + 2]);
			enrySet.add(new AbstractMap.SimpleEntry(key, value));
		}

		return enrySet;
	}

	@Override
	public V get(Object key) {
		QueryBuilder query = new QueryBuilder();
		query.addQuery(Query.HGET);
		query.addParam(Params.KEY, mName);
		query.addParam(Params.FIELD, String.format("%s::%s", key.getClass().getName(), key.toString()));
		
		String responce = Redjs.getTransport().send(query.build());
		if (responce.startsWith("$")) {
			String regexp = "\\$.+\\r\\n(.+)";
			Pattern pattern = Pattern.compile(regexp);
			Matcher matcher = pattern.matcher(responce);

			if (matcher.find()) {
				return (V) parseValue(matcher.group(1));
			}
		} else if (responce.startsWith(":")) {
			String regexp = ":\\r\\n(.+)\\r\\n";
			Pattern pattern = Pattern.compile(regexp);
			Matcher matcher = pattern.matcher(responce);

			if (matcher.find()) {
				return (V) parseValue(matcher.group(1));
			}
		}
		
		return null;
	}

	@Override
	public boolean isEmpty() {
		QueryBuilder query = new QueryBuilder();
		query.addQuery(Query.HGET);
		query.addParam(Params.KEY, mName);
		query.addParam(Params.FIELD, EMPTY_FIELD);

		String responce = Redjs.getTransport().send(query.build());
		return responce.equals("$4\r\ntrue\r\n");
	}

	@Override
	public Set<K> keySet() {
		QueryBuilder query = new QueryBuilder();
		query.addQuery(Query.HKEYS);
		query.addParam(Params.KEY, mName);
		String responce = Redjs.getTransport().send(query.build());

		String[] terms = responce.split("\r\n");
		Set<K> keySet = new HashSet<>();

		for (int i = 2; i < terms.length; i = i + 2) {
			if (terms[i].equals(EMPTY_FIELD)) {
				continue;
			}
			K key = (K) parseValue(terms[i]);
			keySet.add(key);
		}

		return keySet;
	}

	@Override
	public V put(K key, V value) {
		QueryBuilder query = new QueryBuilder();
		query.addQuery(Query.HSET);
		query.addParam(Params.KEY, mName);
		query.addParam(Params.FIELD, String.format("%s::%s", key.getClass().getName(), key.toString()));
		query.addParam(Params.VALUE, String.format("%s::%s", value.getClass().getName(), value.toString()));
		Redjs.getTransport().send(query.build());

		query = new QueryBuilder();
		query.addQuery(Query.HSET);
		query.addParam(Params.KEY, mName);
		query.addParam(Params.FIELD, EMPTY_FIELD);
		query.addParam(Params.VALUE, false);
		Redjs.getTransport().send(query.build());

		return value;
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		for (K key : m.keySet()) {
			put(key, m.get(key));
		}
	}

	@Override
	public V remove(Object key) {
		if (containsKey(key)) {
			V value = get(key);

			QueryBuilder query = new QueryBuilder();
			query.addQuery(Query.HDEL);
			query.addParam(Params.KEY, mName);
			query.addParam(Params.FIELD, key);
			Redjs.getTransport().send(query.build());
			
			if (size() == 0){
				query = new QueryBuilder();
				query.addQuery(Query.HSET);
				query.addParam(Params.KEY, mName);
				query.addParam(Params.FIELD, EMPTY_FIELD);
				query.addParam(Params.VALUE, true);
				Redjs.getTransport().send(query.build());
			}

			return value;
		} else {
			return null;
		}
	}

	@Override
	public int size() {
		QueryBuilder query = new QueryBuilder();
		query.addQuery(Query.HLEN);
		query.addParam(Params.KEY, mName);
		String responce = Redjs.getTransport().send(query.build());

		String regexp = ":(\\d+)\r\n";
		Pattern pattern = Pattern.compile(regexp);
		Matcher matcher = pattern.matcher(responce);

		if (matcher.find()) {
			return Integer.parseInt(matcher.group(1)) - 1;
		} else {
			return 0;
		}
	}

	@Override
	public Collection<V> values() {
		Collection<V> values = new HashSet<>();
		
		for (K key : keySet()){
			values.add(get(key));
		}
		return values;
	}
	
	@SuppressWarnings("unchecked")
	private Object parseValue(String string) {
		try {
			int index = string.indexOf("::");
			String typeName = string.substring(0, index);
			String value = string.substring(index+2);
			
			Constructor<String> constructor = (Constructor<String>) Class.forName(typeName).getConstructor(new Class[]{String.class});
			return constructor.newInstance(value);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
}
