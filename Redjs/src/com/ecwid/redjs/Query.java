package com.ecwid.redjs;

public enum Query {
	SCAN,
	HDEL,
	HEXISTS,
	HGET,
	HGETALL,
	HKEYS,
	HLEN,
	HSET,
	HVALS,
	DEL
}
