package com.ecwid.redjs;

public enum Params {
	KEY,
	FIELD,
	VALUE,
	CURSOR,
	MATCH,
	NAME
}
