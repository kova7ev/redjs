package com.ecwid.redjs;

import com.ecwid.redjs.map.RedjsMaps;
import com.ecwid.redjs.transport.TcpTransport;

public class Redjs {
	private static TcpTransport mTransport;
	private static RedjsMaps mMaps;
	private static boolean mInitialized;
	
	public static void init(String host, int port){
		mTransport = new TcpTransport(host, port);
		mInitialized = true;
	}
	
	public static TcpTransport getTransport(){
		return mTransport;
	}
	
	public static RedjsMaps maps(){
		if (!mInitialized){
			System.out.println("Redjs isn't initialized. Please call first 'Redjs.init(String host, int port)' methods");
			System.exit(-1);
		}
		
		if (mMaps == null){
			mMaps = new RedjsMaps();
		}
		
		return mMaps;
	}
}
