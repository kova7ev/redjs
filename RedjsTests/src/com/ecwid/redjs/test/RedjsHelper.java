package com.ecwid.redjs.test;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class RedjsHelper {
	public static String generateName(int length) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < length; i++) {
			int value = (int) (Math.random() * 10);
			builder.append(value);
		}

		return String.format("record_%s", builder.toString());
	}

	
	public static String send(String host, int port, String query) {

		try {
			Socket socket = new Socket(host, port);

			BufferedWriter writer = new BufferedWriter(new PrintWriter(
					socket.getOutputStream(), true));
			InputStreamReader reader = new InputStreamReader(
					socket.getInputStream());

			writer.write(query);
			writer.flush();

			List<Integer> byteArray = new ArrayList<Integer>();
			int b;

			int timeout = 500;
			int index = 0;
			while (!reader.ready() && index < timeout / 10) {
				Thread.sleep(10);
				index++;
			}

			if (index == timeout / 10) {
				System.err.println("Couldn't get I/O for the connection");
				System.exit(1);
			}

			while (reader.ready() && (b = reader.read()) > -1) {
				byteArray.add(b);
			}

			byte[] buffer = new byte[byteArray.size()];
			for (int i = 0; i < byteArray.size(); i++) {
				buffer[i] = byteArray.get(i).byteValue();
			}

			writer.close();
			reader.close();
			socket.close();

			return new String(buffer);

		} catch (UnknownHostException e) {
			System.err.println("Don't know about host");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for the connection");
			System.exit(1);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}
}
