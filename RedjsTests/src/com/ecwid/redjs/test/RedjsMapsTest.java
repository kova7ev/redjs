package com.ecwid.redjs.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.ecwid.redjs.Redjs;
import com.ecwid.redjs.map.RedjsMap;

public class RedjsMapsTest {
	private String mHost;
	private int mPort;
	private final int NUMBER_ITERATIONS = 10;

	@Before
	public void setUp() throws Exception {
		mHost = "localhost";
		mPort = 6379;

		Redjs.init(mHost, mPort);
	}

	@Test
	public void newMapTest() {
		for (int i = 0; i < NUMBER_ITERATIONS; i++) {
			String recordName = RedjsHelper.generateName(10);

			RedjsMap<String, Object> redjsMap = Redjs.maps().newMap(recordName);
			redjsMap.put("filed#1", 1);
			redjsMap.put("filed#2", 2);
			redjsMap.put("filed#3", 3);

			String query = "*4\r\n$4\r\nSCAN\r\n$1\r\n0\r\n$5\r\nMATCH\r\n$%d\r\n%s\r\n";
			String responce = RedjsHelper.send(mHost, mPort, String.format(query, recordName.length(), recordName));
			String result = String.format("*2\r\n$1\r\n0\r\n*1\r\n$%d\r\n%s\r\n", recordName.length(), recordName);

			RedjsHelper.send(mHost, mPort, String.format("*2\r\n$3\r\nDEL\r\n$%d\r\n%s\r\n",recordName.length(), recordName));
			assertTrue(responce.equals(result));
		}
	}
	
	@Test
	public void existsMapTest() {
		for (int i = 0; i < NUMBER_ITERATIONS / 2; i++) {
			String recordName = RedjsHelper.generateName(10);

			RedjsMap<String, Object> redjsMap = Redjs.maps().newMap(recordName);
			redjsMap.put("filed#1", 1);
			redjsMap.put("filed#2", 2);
			redjsMap.put("filed#3", 3);

			assertTrue(Redjs.maps().existsMap(recordName));			
			RedjsHelper.send(mHost, mPort, String.format("*2\r\n$3\r\nDEL\r\n$%d\r\n%s\r\n",recordName.length(), recordName));
		}
		
		for (int i = 0; i < NUMBER_ITERATIONS / 2; i++) {
			String recordName = RedjsHelper.generateName(10);

			RedjsMap<String, Object> redjsMap = Redjs.maps().newMap(recordName);
			redjsMap.put("filed#1", 1);
			redjsMap.put("filed#2", 2);
			redjsMap.put("filed#3", 3);

			assertFalse(Redjs.maps().existsMap("HashWithThisNameNotExists"));			
			RedjsHelper.send(mHost, mPort, String.format("*2\r\n$3\r\nDEL\r\n$%d\r\n%s\r\n",recordName.length(), recordName));
		}
	}
	
	@Test
	public void getMapTest() {
		for (int i = 0; i < NUMBER_ITERATIONS / 2; i++) {
			String recordName = RedjsHelper.generateName(10);
			
			RedjsMap<String, Object> redjsMapA = Redjs.maps().newMap(recordName);
			redjsMapA.put("filed#1", 1);
			redjsMapA.put("filed#2", 2);
			redjsMapA.put("filed#3", 3);

			RedjsMap<String, Object> redjsMapB = Redjs.maps().getMap(recordName);

			assertTrue(redjsMapB != null);
			RedjsHelper.send(mHost, mPort, String.format("*2\r\n$3\r\nDEL\r\n$%d\r\n%s\r\n",recordName.length(), recordName));
		}
		
		for (int i = 0; i < NUMBER_ITERATIONS / 2; i++) {
			String recordName = RedjsHelper.generateName(10);
			RedjsMap<String, Object> redjsMap = Redjs.maps().getMap(recordName);
			assertTrue(redjsMap == null);
		}
	}
	
	@Test
	public void deleteMapByNameTest() {
		for (int i = 0; i < NUMBER_ITERATIONS / 2; i++) {
			String recordName = RedjsHelper.generateName(10);
			
			RedjsMap<String, Object> redjsMap = Redjs.maps().newMap(recordName);
			redjsMap.put("filed#1", 1);
			redjsMap.put("filed#2", 2);
			redjsMap.put("filed#3", 3);

			assertTrue(Redjs.maps().deleteMap(recordName));
			RedjsHelper.send(mHost, mPort, String.format("*2\r\n$3\r\nDEL\r\n$%d\r\n%s\r\n",recordName.length(), recordName));
		}
		
		for (int i = 0; i < NUMBER_ITERATIONS / 2; i++) {
			String recordName = RedjsHelper.generateName(10);
			
			RedjsMap<String, Object> redjsMap = Redjs.maps().newMap(recordName);
			redjsMap.put("filed#1", 1);
			redjsMap.put("filed#2", 2);
			redjsMap.put("filed#3", 3);

			assertTrue(!Redjs.maps().deleteMap("HashWithThisNameNotExists"));
			RedjsHelper.send(mHost, mPort, String.format("*2\r\n$3\r\nDEL\r\n$%d\r\n%s\r\n",recordName.length(), recordName));
		}
	}
	
	@Test
	public void deleteMapRefTest() {
		for (int i = 0; i < NUMBER_ITERATIONS / 2; i++) {
			String recordName = RedjsHelper.generateName(10);
			
			RedjsMap<String, Object> redjsMap = Redjs.maps().newMap(recordName);
			redjsMap.put("filed#1", 1);
			redjsMap.put("filed#2", 2);
			redjsMap.put("filed#3", 3);

			assertTrue(Redjs.maps().deleteMap(redjsMap));
			RedjsHelper.send(mHost, mPort, String.format("*2\r\n$3\r\nDEL\r\n$%d\r\n%s\r\n",recordName.length(), recordName));
		}
	}
}