package com.ecwid.redjs.test;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.ecwid.redjs.Redjs;
import com.ecwid.redjs.map.RedjsMap;

public class RedjsMapTest {
	private String mHost;
	private int mPort;
	private final int NUMBER_ITERATIONS = 10;

	@Before
	public void setUp() throws Exception {
		mHost = "localhost";
		mPort = 6379;

		Redjs.init(mHost, mPort);
	}

	@Test
	public void RedjsMapConstructorTest() {
		for (int i = 0; i < NUMBER_ITERATIONS; i++) {
			String recordName = RedjsHelper.generateName(10);
			Redjs.maps().newMap(recordName);
			String query = "*2\r\n$7\r\nHGETALL\r\n$%d\r\n%s\r\n";
			String response = RedjsHelper.send(mHost, mPort, String.format(query, recordName.length(), recordName));
			assertTrue(response.equals("*2\r\n$8\r\nis_empty\r\n$4\r\ntrue\r\n"));
			Redjs.maps().deleteMap(recordName);
		}
	}
	
	@Test
	public void RedjsMapGetNameTest() {
		for (int i = 0; i < NUMBER_ITERATIONS; i++) {
			String recordName = RedjsHelper.generateName(10);
			RedjsMap<String, Object> redjsMap = Redjs.maps().newMap(recordName);
			assertTrue(recordName.equals(redjsMap.getName()));
			Redjs.maps().deleteMap(redjsMap);
		}
	}
	
	@Test
	public void RedjsMapClearTest() {
		for (int i = 0; i < NUMBER_ITERATIONS; i++) {
			String recordName = RedjsHelper.generateName(10);
			String query = "*10\r\n$5\r\nHMSET\r\n$%d\r\n%s\r\n$8\r\nis_empty\r\n$5\r\nfalse\r\n$7\r\nfield#1\r\n$1\r\n#\r\n$7\r\nfield#2\r\n$2\r\n##\r\n$7\r\nfield#3\r\n$3\r\n###\r\n";
			RedjsHelper.send(mHost, mPort, String.format(query, recordName.length(), recordName));
			RedjsMap<String, Object> redjsMap = Redjs.maps().getMap(recordName);
			assertTrue(redjsMap != null);
			redjsMap.clear();
			query = "*2\r\n$7\r\nHGETALL\r\n$%d\r\n%s\r\n";
			String response = RedjsHelper.send(mHost, mPort, String.format(query, recordName.length(), recordName));
			assertTrue(response.equals("*2\r\n$8\r\nis_empty\r\n$4\r\ntrue\r\n"));
			Redjs.maps().deleteMap(recordName);
		}
	}
	
	@Test
	public void RedjsContainsKeyTest() {
		for (int i = 0; i < NUMBER_ITERATIONS; i++) {
			String recordName = RedjsHelper.generateName(10);
			String query = "*10\r\n$5\r\nHMSET\r\n$%d\r\n%s\r\n$8\r\nis_empty\r\n$5\r\nfalse\r\n$7\r\nfield#1\r\n$1\r\n#\r\n$7\r\nfield#2\r\n$2\r\n##\r\n$7\r\nfield#3\r\n$3\r\n###\r\n";
			RedjsHelper.send(mHost, mPort, String.format(query, recordName.length(), recordName));
			RedjsMap<String, Object> redjsMap = Redjs.maps().getMap(recordName);
			
			assertTrue(redjsMap != null);
			assertTrue(redjsMap.containsKey("field#1"));
			assertTrue(redjsMap.containsKey("field#2"));
			assertTrue(redjsMap.containsKey("field#3"));
			assertTrue(!redjsMap.containsKey("field#4"));
			
			Redjs.maps().deleteMap(recordName);
		}
	}
	
	@Test
	public void RedjsContainsValueTest() {
		for (int i = 0; i < NUMBER_ITERATIONS; i++) {
			String recordName = RedjsHelper.generateName(10);
			String query = "*10\r\n$5\r\nHMSET\r\n$%d\r\n%s\r\n$8\r\nis_empty\r\n$5\r\nfalse\r\n$7\r\nfield#1\r\n$1\r\n#\r\n$7\r\nfield#2\r\n$2\r\n##\r\n$7\r\nfield#3\r\n$3\r\n###\r\n";
			RedjsHelper.send(mHost, mPort, String.format(query, recordName.length(), recordName));
			RedjsMap<String, Object> redjsMap = Redjs.maps().getMap(recordName);
			
			assertTrue(redjsMap != null);
			assertTrue(redjsMap.containsValue("#"));
			assertTrue(redjsMap.containsValue("##"));
			assertTrue(redjsMap.containsValue("###"));
			assertTrue(!redjsMap.containsValue("!#"));
			
			Redjs.maps().deleteMap(recordName);
		}
	}
	
	@Test
	public void RedjsEntrySetTest() {
		for (int i = 0; i < NUMBER_ITERATIONS; i++) {
			String recordName = RedjsHelper.generateName(10);
			String query = "*10\r\n$5\r\nHMSET\r\n$%d\r\n%s\r\n$8\r\nis_empty\r\n$5\r\nfalse\r\n$25\r\njava.lang.String::field#1\r\n$19\r\njava.lang.String::#\r\n$25\r\njava.lang.String::field#2\r\n$20\r\njava.lang.String::##\r\n$25\r\njava.lang.String::field#3\r\n$21\r\njava.lang.String::###\r\n";
			RedjsHelper.send(mHost, mPort, String.format(query, recordName.length(), recordName));
			RedjsMap<String, String> redjsMap = Redjs.maps().getMap(recordName);
			
			assertTrue(redjsMap != null);
			Set<java.util.Map.Entry<String, String>> entrySet = redjsMap.entrySet();
			
			assertTrue(entrySet.size() == 3);
			for (java.util.Map.Entry<String, String> entry : entrySet){
				assertTrue(entry.getKey().contains("field#") && entry.getValue().contains("#"));
			}
			
			Redjs.maps().deleteMap(recordName);
		}
	}
	
	@Test
	public void RedjsGetTest() {
		for (int i = 0; i < NUMBER_ITERATIONS; i++) {
			String recordName = RedjsHelper.generateName(10);
			String query = "*10\r\n$5\r\nHMSET\r\n$%d\r\n%s\r\n$8\r\nis_empty\r\n$5\r\nfalse\r\n$25\r\njava.lang.String::field#1\r\n$19\r\njava.lang.String::#\r\n$25\r\njava.lang.String::field#2\r\n$20\r\njava.lang.String::##\r\n$25\r\njava.lang.String::field#3\r\n$21\r\njava.lang.String::###\r\n";
			RedjsHelper.send(mHost, mPort, String.format(query, recordName.length(), recordName));
			RedjsMap<String, Object> redjsMap = Redjs.maps().getMap(recordName);
			
			assertTrue(redjsMap != null);
			assertTrue(redjsMap.get("field#1").equals("#"));
			assertTrue(redjsMap.get("field#2").equals("##"));
			assertTrue(redjsMap.get("field#3").equals("###"));
			assertTrue(redjsMap.get("field#4") == null);
			
			Redjs.maps().deleteMap(recordName);
		}
	}
	
	@Test
	public void RedjsIsEmptyTest() {
		for (int i = 0; i < NUMBER_ITERATIONS/2; i++) {
			String recordName = RedjsHelper.generateName(10);
			RedjsMap<String, Object> redjsMap = Redjs.maps().newMap(recordName);
			
			assertTrue(redjsMap != null);
			assertTrue(redjsMap.isEmpty());
			
			Redjs.maps().deleteMap(recordName);
		}
		
		for (int i = 0; i < NUMBER_ITERATIONS; i++) {
			String recordName = RedjsHelper.generateName(10);
			String query = "*10\r\n$5\r\nHMSET\r\n$%d\r\n%s\r\n$8\r\nis_empty\r\n$5\r\nfalse\r\n$25\r\njava.lang.String::field#1\r\n$19\r\njava.lang.String::#\r\n$25\r\njava.lang.String::field#2\r\n$20\r\njava.lang.String::##\r\n$25\r\njava.lang.String::field#3\r\n$21\r\njava.lang.String::###\r\n";
			RedjsHelper.send(mHost, mPort, String.format(query, recordName.length(), recordName));
			RedjsMap<String, Object> redjsMap = Redjs.maps().getMap(recordName);
			
			assertTrue(redjsMap != null);
			assertTrue(!redjsMap.isEmpty());
			
			Redjs.maps().deleteMap(recordName);
		}
	}
	
	@Test
	public void RedjsKeySetTest() {
		for (int i = 0; i < NUMBER_ITERATIONS; i++) {
			String recordName = RedjsHelper.generateName(10);
			String query = "*10\r\n$5\r\nHMSET\r\n$%d\r\n%s\r\n$8\r\nis_empty\r\n$5\r\nfalse\r\n$25\r\njava.lang.String::field#1\r\n$19\r\njava.lang.String::#\r\n$25\r\njava.lang.String::field#2\r\n$20\r\njava.lang.String::##\r\n$25\r\njava.lang.String::field#3\r\n$21\r\njava.lang.String::###\r\n";
			RedjsHelper.send(mHost, mPort, String.format(query, recordName.length(), recordName));
			RedjsMap<String, String> redjsMap = Redjs.maps().getMap(recordName);
			
			assertTrue(redjsMap != null);
			
			Set<String> keySet = redjsMap.keySet();
			
			assertTrue(keySet.size() == 3);
			for (String key : keySet){
				assertTrue(key.contains("field#"));
			}
			
			Redjs.maps().deleteMap(recordName);
		}
	}
	
	@Test
	public void RedjsPutTest(){
		for (int i = 0; i < NUMBER_ITERATIONS; i++) {
			String recordName = RedjsHelper.generateName(10);
			RedjsMap<String, Integer> redjsMap = Redjs.maps().newMap(recordName);
			
			for (int j = 0; j < 3; j++){
				redjsMap.put(String.format("field#%s", j), j);
			}
			
			String query = "*2\r\n$7\r\nHGETALL\r\n$%d\r\n%s\r\n";
			String response = RedjsHelper.send(mHost, mPort, String.format(query, recordName.length(), recordName));
			assertTrue(response.equals("*8\r\n$8\r\nis_empty\r\n$5\r\nfalse\r\n$25\r\njava.lang.String::field#0\r\n$20\r\njava.lang.Integer::0\r\n$25\r\njava.lang.String::field#1\r\n$20\r\njava.lang.Integer::1\r\n$25\r\njava.lang.String::field#2\r\n$20\r\njava.lang.Integer::2\r\n"));
			
			Redjs.maps().deleteMap(recordName);
		}
	}
	
	@Test
	public void RedjsPutAllTest(){
		Map<String, Object> employer = new HashMap<String, Object>();
		employer.put("name", "Henry");
		employer.put("surname", "Letham");
		employer.put("age", 21);
		employer.put("location", "USA");
		
		for (int i = 0; i < NUMBER_ITERATIONS; i++) {
			String recordName = RedjsHelper.generateName(10);
			RedjsMap<String, Object> redjsMap = Redjs.maps().newMap(recordName);
			
			assertTrue(redjsMap != null);
			redjsMap.putAll(employer);
			
			String query = "*2\r\n$7\r\nHGETALL\r\n$%d\r\n%s\r\n";
			String response = RedjsHelper.send(mHost, mPort, String.format(query, recordName.length(), recordName));
			assertTrue(response.equals("*10\r\n$8\r\nis_empty\r\n$5\r\nfalse\r\n$26\r\njava.lang.String::location\r\n$21\r\njava.lang.String::USA\r\n$21\r\njava.lang.String::age\r\n$21\r\njava.lang.Integer::21\r\n$22\r\njava.lang.String::name\r\n$23\r\njava.lang.String::Henry\r\n$25\r\njava.lang.String::surname\r\n$24\r\njava.lang.String::Letham\r\n"));
			
			Redjs.maps().deleteMap(recordName);
		}
	}
	
	@Test
	public void RedjsRemoveTest(){
		for (int i = 0; i < NUMBER_ITERATIONS; i++) {
			String recordName = RedjsHelper.generateName(10);
			String query = "*10\r\n$5\r\nHMSET\r\n$%d\r\n%s\r\n$8\r\nis_empty\r\n$5\r\nfalse\r\n$7\r\nfield#1\r\n$19\r\njava.lang.String::#\r\n$7\r\nfield#2\r\n$20\r\njava.lang.String::##\r\n$7\r\nfield#3\r\n$21\r\njava.lang.String::###\r\n";
			RedjsHelper.send(mHost, mPort, String.format(query, recordName.length(), recordName));
			RedjsMap<String, Object> redjsMap = Redjs.maps().getMap(recordName);
			
			assertTrue(redjsMap != null);
			
			redjsMap.remove("field#1");
			redjsMap.remove("field#2");
			redjsMap.remove("field#3");
			
			assertTrue(redjsMap.isEmpty());
			Redjs.maps().deleteMap(recordName);
		}
	}
	
	@Test
	public void RedjsSizeTest(){
		for (int i = 0; i < NUMBER_ITERATIONS; i++) {
			String recordName = RedjsHelper.generateName(10);
			String query = "*10\r\n$5\r\nHMSET\r\n$%d\r\n%s\r\n$8\r\nis_empty\r\n$5\r\nfalse\r\n$25\r\njava.lang.String::field#1\r\n$19\r\njava.lang.String::#\r\n$25\r\njava.lang.String::field#2\r\n$20\r\njava.lang.String::##\r\n$25\r\njava.lang.String::field#3\r\n$21\r\njava.lang.String::###\r\n";
			RedjsHelper.send(mHost, mPort, String.format(query, recordName.length(), recordName));
			RedjsMap<String, Object> redjsMap = Redjs.maps().getMap(recordName);
			
			assertTrue(redjsMap != null);
			assertTrue(redjsMap.size() == 3);
			Redjs.maps().deleteMap(recordName);
		}
	}
	
	@Test
	public void RedjsValuesTest(){
		for (int i = 0; i < NUMBER_ITERATIONS; i++) {
			String recordName = RedjsHelper.generateName(10);
			String query = "*10\r\n$5\r\nHMSET\r\n$%d\r\n%s\r\n$8\r\nis_empty\r\n$5\r\nfalse\r\n$25\r\njava.lang.String::field#1\r\n$19\r\njava.lang.String::#\r\n$25\r\njava.lang.String::field#2\r\n$20\r\njava.lang.String::##\r\n$25\r\njava.lang.String::field#3\r\n$21\r\njava.lang.String::###\r\n";
			RedjsHelper.send(mHost, mPort, String.format(query, recordName.length(), recordName));
			RedjsMap<String, String> redjsMap = Redjs.maps().getMap(recordName);
			
			assertTrue(redjsMap != null);
			Collection<String> values = redjsMap.values();
			assertTrue(values.contains("#") && values.contains("##") 
					&& values.contains("###") && !values.contains("!#"));
			
			Redjs.maps().deleteMap(recordName);
		}
	}
}
