package com.ecwid.redjs.sample;

import com.ecwid.redjs.Redjs;
import com.ecwid.redjs.map.RedjsMap;

public class Main {
	public static void main(String[] args){
		Redjs.init("localhost", 6379);
		
		RedjsMap<String, Object> redjsEmployer = Redjs.maps().newMap("employer#1");
		redjsEmployer.put("name", "Henry");
		redjsEmployer.put("surname", "Letham");
		redjsEmployer.put("age", 21);
		redjsEmployer.put("location", "USA");
		
		for (String key : redjsEmployer.keySet()){
			System.out.println(String.format("%s: %s", key, redjsEmployer.get(key).toString()));
		}
	}
}
